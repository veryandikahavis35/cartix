import { Module } from '@nestjs/common';
import { EventService } from './events.service';
import { EventController } from './events.controller';
import { Events } from './entity/events.entity';
import { TypeOrmModule } from '@nestjs/typeorm';
import { Role } from 'src/users/entities/role.entity';

@Module({
  imports: [TypeOrmModule.forFeature([Events, Role])],
  providers: [EventService],
  controllers: [EventController]
})
export class EventsModule {}
