import { Body, Controller, Delete, Get, HttpStatus, Param, ParseUUIDPipe, Post, Put, Query } from '@nestjs/common';
import { request } from 'http';
import { CreateEventsDto } from './dto/create.events.dto';
import { UpdateEventsDto } from './dto/edit.events.dto';
import { EventService } from './events.service';

@Controller('event')
export class EventController {
    constructor(private readonly eventService: EventService) {}

    @Get ()
    getAllEvents (
        @Query('namaEvent') namaEvent: string
        ) {
        return this.eventService.getAllEvents();
    }

    @Get('/id_events')
    getEvent(@Param('id_events') id_events: string) {
        return this.eventService.getEvent(id_events);
    }


    @Post ('/create')
    async createEvent (@Body() request: CreateEventsDto) {
            try{
            await this.eventService.createEvent(request);

                return {
                    statusCode: 200,
                    message: 'berhasil membuat event'
                }
            }catch(error){
                return {
                    message: 'error di ', error
                }
            }
    }


    @Put('/:id_events')
    async updateEvent(
            @Param('id_events', ParseUUIDPipe) id_events: string,
            @Body() updateEventsDto: UpdateEventsDto,
        ) {
            return {
                data: await this.eventService.updateEvent(id_events, updateEventsDto),
                statusCode: HttpStatus.OK,
                message: 'success'
            };
        //     try{
        //         await this.eventService.updateEvent(id_events, createEventsDto)
        //     return{
        //             statusCode: HttpStatus.OK,
        //             message: 'berhasil mengedit event',
        //     }
        // }catch (error) {
        //        return{ 
        //             message: 'error di ',
        //             error
        //     }
        
        
    }

    @Delete('/:id') 
    deleteEvent(@Param('id') id: string){
        return this.eventService.deleteEvent(id);
    }
}
