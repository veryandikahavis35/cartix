import { Column, CreateDateColumn, DeleteDateColumn, Entity, PrimaryGeneratedColumn, UpdateDateColumn } from "typeorm";

@Entity()
export class Events {
    @PrimaryGeneratedColumn('uuid')
    id_events: string;

    @Column()
    nama_event: string;

    @Column({type: 'date', nullable: true})
    tanggal_waktu_event: Date;

    @Column()
    deskripsi: string;
    
    @Column({nullable: true})
    detail: string;

    @Column({nullable: true})
    banner_event: string;

    @Column({nullable: true})
    jumlah_tiket_event: number;


  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;
}