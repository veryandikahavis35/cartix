import { HttpException, HttpStatus, Injectable, NotFoundException } from '@nestjs/common';
import { InjectRepository } from '@nestjs/typeorm';
import { EntityNotFoundError, Repository } from 'typeorm';
import { v4 as uuidv4 } from 'uuid'
import { CreateEventsDto } from './dto/create.events.dto';
import { UpdateEventsDto } from './dto/edit.events.dto';
import { Events } from './entity/events.entity';
@Injectable()
export class EventService {
    constructor(
        @InjectRepository(Events)
        private eventRepository: Repository<Events>,
        // private events: any[] = []
        ) { }
        
    async createEvent(createEventsDto: CreateEventsDto) {
        const result = await this.eventRepository.insert
        (createEventsDto);    
        return this.eventRepository.findOneOrFail({
            where: {
                id_events: result.identifiers[0].id_events,
            },
        });
    }

    getAllEvents() {
        return this.eventRepository.findAndCount();
    }


    

    async getEvent(nama_event: string) {
        try{
            return await this.eventRepository.findOneOrFail({
                where: {
                    nama_event,
                },
            });
        } catch (error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data not found',
                    },
                    HttpStatus.NOT_FOUND,
                );
            } else {
                throw error;
            }
        }
    }



    async updateEvent(id_events: string,request: UpdateEventsDto){
        try{
            await this.eventRepository.findOneOrFail({
                where: {
                  id_events,
                },
              });
            const data = new Events()
            data.nama_event = request.nama_event;
            data.deskripsi = request.deskripsi;
            data.detail = request.detail;
            data.banner_event = request.banner_event;
            data.jumlah_tiket_event = request.jumlah_tiket_event;
            
            await this.eventRepository.insert(data)
            
    }catch (error) {
            throw error;
    }
    }
    

    // findEventById(id: string){
    //     const eventIndex = this.events.findIndex((event) => event.id === id);
    //     if (eventIndex === -1) {
    //         throw new NotFoundException(`event id ${id} is not found`);
    //     }
    //     return eventIndex;
    // }

    async deleteEvent(id_events: string){
        try {
            await this.eventRepository.findOneOrFail({
                where: {
                    id_events,
                },
            });
        } catch (error) {
            if (error instanceof EntityNotFoundError) {
                throw new HttpException(
                    {
                        statusCode: HttpStatus.NOT_FOUND,
                        error: 'Data not Found',
                    },
                    HttpStatus.NOT_FOUND,
                );
            } else {
                throw error;
            }
        }

        await this.eventRepository.delete(id_events);
    }
}
