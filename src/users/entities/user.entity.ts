import {
  Entity,
  Column,
  PrimaryGeneratedColumn,
  UpdateDateColumn,
  DeleteDateColumn,
  VersionColumn,
  CreateDateColumn,
  ManyToOne,
} from 'typeorm';
import { Role } from './role.entity';

@Entity()
export class Users {
  @PrimaryGeneratedColumn('uuid')
  id_users: string;

  @Column()
  username: string;

  
  @Column({nullable: true})
  gambar: string;
  
  @Column({unique: true})
  email: string;
  
  @Column()
  password: string;
  
  @Column()
  hash: string;
  
  @ManyToOne(
    () => {
      return Role;
    },
    (callBack) => {
      return callBack.id_role;
    }
  )

  role: number;
  
  
  @Column({unique: true})
  no_telepon: string;
  
  @Column({nullable: true})
  alamat: string;
  
  @Column({nullable: true})
  jenis_kelamin: string;

  @Column({type: 'varchar'})
  tanggal_lahir: number;

  @CreateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  createdAt: Date;

  @UpdateDateColumn({
    type: 'timestamp',
    nullable: false,
  })
  updatedAt: Date;

  @DeleteDateColumn({
    type: 'timestamp',
    nullable: true,
  })
  deletedAt: Date;

  
}
