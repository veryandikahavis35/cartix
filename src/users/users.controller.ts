import {
  Controller,
  Get,
  Post,
  Body,
  Put,
  Param,
  Delete,
  ParseUUIDPipe,
  HttpStatus,
} from '@nestjs/common';
import { UsersService } from './users.service';
import { CreateUserDto } from './dto/create-user.dto';
import { UpdateUserDto } from './dto/update-user.dto';

@Controller('users')
export class UsersController {
  constructor(private readonly usersService: UsersService) {}

  @Post()
  async create(@Body() createUserDto: CreateUserDto) {
    return {
      data: await this.usersService.create(createUserDto),
      statusCode: HttpStatus.CREATED,
      message: 'success',
    };
  }

  @Get()
  async findAll() {
    const [data, count] = await this.usersService.findAll();

    return {
      data,
      count,
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Get(':id_users')
  async findOne(@Param('id_users', ParseUUIDPipe) id_users: string) {
    return {
      data: await this.usersService.findOne(id_users),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Put(':id_users')
  async update(
    @Param('id', ParseUUIDPipe) id_users: string,
    @Body() updateUserDto: UpdateUserDto,
  ) {
    return {
      data: await this.usersService.update(id_users, updateUserDto),
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }

  @Delete(':id_users')
  async remove(@Param('id_users', ParseUUIDPipe) id_users: string) {
    await this.usersService.remove(id_users);

    return {
      statusCode: HttpStatus.OK,
      message: 'success',
    };
  }
}
